## MyList
### Description (RU)
Реализовать класс List на основании массива объектов. Класс должен иметь
методы Add, Clear, Contains, Remove, RemoveAt, IndexOf, ElementAt(Получение элемента по
индексу) и свойства Count, Capacity.