﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyList
{
    /// <summary>
    /// Класс представляет собой Лист основанный на массиве.
    /// </summary>
    /// <typeparam name="Type">Тип данных в листе.</typeparam>
    public class MyList<Type>
    {
        private Type[] _data;
        private int _count;
        private int _capacity;

        /// <summary>
        /// Создает новый объект класса <see cref="MyList" />
        /// </summary>
        /// <param name="cap">Вместимость</param>
        public MyList(int capacity = 16)
        {
            _capacity = capacity;
            _count = 0;
            _data = new Type[capacity];
        }


        /// <summary>
        /// Добавляет элемент el в список
        /// </summary>
        /// <param name="el">Элемент</param>
        /// <returns>void</returns>
        public void Add(Type el)
        {
            if (isAvailable())
            {
                _data[_count] = el;
            }
            else
            {
                reCreate(true);
                _data[_count] = el;
            }
            _count++;
        }


        /// <summary>
        /// Очищает список.
        /// </summary>
        /// <returns>void</returns>
        public void Clear()
        {
            _data = new Type[_capacity];
        }


        /// <summary>
        /// Проверяет, [содержится] ли [элемент] в списке.
        /// </summary>
        /// <param name="el">Искомый элемент</param>
        /// <returns>
        ///   <c>true</c> если [содержится]; иначе, <c>false</c>.
        /// </returns>
        public bool Contains(Type el)
        {
            if (IndexOf(el) != -1)
                return true;
            else
                return false;
        }


        /// <summary>
        /// Удаляет элемент
        /// </summary>
        /// <param name="el">Элемент</param>
        /// <returns>
        ///   <c>true</c> если [удален]; иначе, <c>false</c>.
        /// </returns>
        public bool Remove(Type el)
        {
            if (Contains(el))
            {
                for (int i = IndexOf(el), len = _count - 1; i < len; i++)
                {
                    _data[i] = _data[i+1];
                }
                if (!isAvailable())
                    reCreate(true);
                _count--;
                return true;
            }
            else return false;
        }


        /// <summary>
        /// Удаляет элемент на конкретной позиции
        /// </summary>
        /// <param name="pos">Позиция для удаления.</param>
        /// <returns>
        ///   <c>true</c> если [удален]; иначе, <c>false</c>.
        /// </returns>
        public bool RemoveAt(uint pos)
        {
            if (pos < _count)
                return Remove(ElementAt(pos));
            else
                return false;
        }


        /// <summary>
        /// Ищет позицию элемента в списке..
        /// </summary>
        /// <param name="el">Искомый элемент.</param>
        /// <returns>
        /// Возвращает позицию элемента или -1, если он не найден.
        /// </returns>
        public int IndexOf(Type el)
        {
            for (int i = 0, len = _count; i < len; i++)
            {
                if (el.Equals(_data[i])) return i;
            }
            return -1;
        }


        /// <summary>
        /// Возвращает элемент по позиции в списке.
        /// </summary>
        /// <param name="pos">Позиция.</param>
        /// <returns>
        ///     Возвращает элемент, иначе дефолтный элемент типа.
        /// </returns>
        public Type ElementAt(uint pos)
        {
            if (pos < _count)
                return _data[pos];
            else
                return default(Type);
        }


        /// <summary>
        /// Количество элементов в списке
        /// </summary>
        /// <value>
        /// Количество
        /// </value>
        public int Count
        {
            get
            {
                return _count;
            }
        }


        /// <summary>
        /// Вместимость листа
        /// </summary>
        /// <value>
        /// Вместимость
        /// </value>
        public int Capacity
        {
            get
            {
                return _capacity;
            }
        }


        /// <summary>
        /// Возвращает, останется ли место, после операции
        /// </summary>
        /// <returns>
        ///   <c>true</c> если место доступно; иначе, <c>false</c>.
        /// </returns>
        private bool isAvailable()
        {
            if (_count % _capacity == 0 && _count != 0)
                return false;
            else
                return true;
        }


        /// <summary>
        /// Перевыделение памяти
        /// </summary>
        /// <param name="type">Если [тип] операции - добавление, то <c>true</c>; иначе <c>false</c>.</param>
        /// <returns>void</returns>
        private void reCreate(bool type)
        {
            int size = (type) ? (_count + 2) / _capacity : (_count - 1) / _capacity;
            Type[] data = _data;
            _data = new Type[size * _capacity];
            for (int i = 0, len = _count; i < len; i++)
            {
                _data[i] = data[i];
            }
        }


        /// <summary>
        /// Возвращает <see cref="System.String" /> описывающий лист.
        /// </summary>
        /// <returns>
        /// <see cref="System.String" />.
        /// </returns>
        public override string ToString()
        {
            StringBuilder res = new StringBuilder();
            for (int i = 0, len = _count - 1; i < len; i++)
            {
                res.Append(_data[i].ToString());
                res.Append(", ");
            }
            res.Append(_data[_count - 1].ToString());
            res.Append(';');
            return res.ToString();
        }
    }
}
