﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyList
{
    class Program
    {
        static void Main(string[] args)
        {
            MyList<int> temp = new MyList<int>(2);
            temp.Add(2);
            temp.Add(3);
            temp.Add(4);
            Console.WriteLine(String.Format("Количество элементов - {0}", temp.Count));
            temp.Add(5);
            temp.Remove(5);
            temp.Add(6);
            temp.Add(7);
            Console.WriteLine(String.Format("Вместимость (?) - {0}", temp.Capacity));
            temp.Add(8);
            temp.RemoveAt(5);

            Console.WriteLine(temp);

            Console.ReadKey();
        }
    }
}
